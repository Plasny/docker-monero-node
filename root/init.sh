#!/bin/bash

mkdir -p /storage/{blockchain,monero-tor-service}
chmod 700 /storage/monero-tor-service

echo -e "\n**********************************************************************"
echo -e "Node URL: "
cat /storage/monero-tor-service/hostname
echo -e "**********************************************************************\n"

tor &
monerod --config-file=/etc/monero/monerod.conf --non-interactive --rpc-login $username:$password
