FROM ubuntu:22.04

RUN apt update && apt install -y tor monero

COPY root/ /

ENTRYPOINT ["/init.sh"]
