# Monero node over tor docker conatiner

The title describes what it does.

---

#### Environment variables:

  - username
  - password

#### Volumes:

  - /storage/monero-tor-service - tor hidden service files
  - /storage/blockchain - blockchain database (this gets big)

---

### Getting the container

#### Building it locally

```sh
docker-compose build
```

#### Pulling it from gitlab

```sh
docker pull registry.gitlab.com/plasny/docker-monero-node:latest
```
